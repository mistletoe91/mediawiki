<?php
require_once("dbcontroller.php");
require_once("pagination.class.php");
$db_handle = new DBController();
$perPage = new PerPage();

    $_POST = array_map('mysql_real_escape_string',$_POST);
    $_GET = array_map('mysql_real_escape_string',$_GET);


$custom_search_text_all = trim($_GET ['custom_search_text_all']);
$custom_search_case =  trim($_GET ['custom_search_case']);
$custom_search_text_exact =  trim($_GET ['custom_search_text_exact']);
$custom_search_text_any =  trim($_GET ['custom_search_text_any']);
$custom_search_text_none =  trim($_GET ['custom_search_text_none']);
$custom_cat =  trim($_GET ['custom_cat']);
$custom_search_for =  trim($_GET ['custom_search_for']);
$custom_search_in =  trim($_GET ['custom_search_in']);
$custom_sort_by =  trim($_GET ['custom_sort_by']);
$custom_sort_order =  trim($_GET ['custom_sort_order']);

$where =' WHERE oo_searchcache.id > 0 ';
$lowertagstart_ = '';
$lowertagend_ = '';
$binarySearch = 'binary';

if(!$custom_search_case){
    $lowertagstart_ = 'LOWER(';
    $lowertagend_ = ')';
    $binarySearch = '';
    $custom_search_text_all = strtolower($custom_search_text_all);
    $custom_search_text_exact = strtolower($custom_search_text_exact);
    $custom_search_text_any = strtolower($custom_search_text_any);
    $custom_search_text_none = strtolower($custom_search_text_none);
    $custom_cat = strtolower($custom_cat);



}#endif 

$custom_search_text_all_arr = explode(" ",$custom_search_text_all);
foreach($custom_search_text_all_arr as $i=>$v){
  if($v){
    $where_blob = '';
    if($custom_search_in){
          $where_blob = "OR (".$lowertagstart_."oo_searchcache_blob.old_text".$lowertagend_." like $binarySearch '%".$v."%')";
    }
    $where .= " AND ((".$lowertagstart_."oo_searchcache.page_title".$lowertagend_." like $binarySearch '%".$v."%')   $where_blob) ";
  }
}

$custom_search_text_none_arr = explode(" ",$custom_search_text_none);
foreach($custom_search_text_none_arr as $i=>$v){
  if($v){
    $where_blob = '';
    if($custom_search_in){
          $where_blob = "OR (".$lowertagstart_."oo_searchcache_blob.old_text".$lowertagend_." NOT like $binarySearch '%".$v."%')";
    }
    $where .= " AND ((".$lowertagstart_."oo_searchcache.page_title".$lowertagend_." NOT like $binarySearch '%".$v."%') OR (oo_searchcache.page_title IS NULL) $where_blob ) ";
  }
}


if($custom_search_text_exact){
    $custom_search_text_exact = preg_replace('/\s+/', '_', $custom_search_text_exact);
    $where_blob = '';
    if($custom_search_in){
          $where_blob = "OR (".$lowertagstart_."oo_searchcache_blob.old_text".$lowertagend_."  like $binarySearch '%".$custom_search_text_exact."%')";
    }
    $where .= " AND ((".$lowertagstart_."oo_searchcache.page_title".$lowertagend_." like $binarySearch '%".$custom_search_text_exact."%') $where_blob ) ";
}

$custom_search_text_any_arr = explode(" ",$custom_search_text_any);
$whereInner = '';
foreach($custom_search_text_any_arr as $i=>$v){
    if($whereInner){
         $whereInner .= " OR ";
    }
    if($v){
          $where_blob = '';
          if($custom_search_in){
                $where_blob = "OR (".$lowertagstart_."oo_searchcache_blob.old_text".$lowertagend_."  like $binarySearch '%".$v."%')";
          }
          $whereInner .= " ((".$lowertagstart_."oo_searchcache.page_title".$lowertagend_." like $binarySearch '%".$v."%') $where_blob ) ";
    }
}
if($whereInner){
    $where .= " AND ( $whereInner )";
}


if($custom_cat){
    #$custom_cat = preg_replace('/\s+/', '_', $custom_cat);
    $where .= " AND (cast(".$lowertagstart_."oo_categorylinks.cl_to".$lowertagend_." as CHAR) like $binarySearch '%".$custom_cat."%') ";
}

if($custom_search_for){
  if (filter_var($custom_search_for, FILTER_VALIDATE_INT) || $custom_search_for >=0) {
    if($custom_search_for != 999){
         $where .= " AND ( page_namespace = $custom_search_for ) ";
    } 
  }
}

$sortby_ = '';
if($custom_sort_by){
         $sortby_ .= " ORDER BY $custom_sort_by ";
}

if($custom_sort_order){
    if($sortby_){
         $sortby_ .= $custom_sort_order. " ";
    }
}

$innerjoin = '';
if($custom_search_in){
    $innerjoin = "inner join oo_searchcache_blob on oo_searchcache.id = oo_searchcache_blob.oo_searchcache_id";
}


$sql = "SELECT  DISTINCT
REPLACE(oo_searchcache.page_title,'_',' ') as page_title,
oo_searchcache.page_title as page_title_link,
page_namespace,
oo_searchcache.rev_timestamp ,
oo_searchcache.rev_timestamp_created ,
oo_searchcache.created_user_text ,
oo_searchcache.rev_user_text,
cast(oo_categorylinks.cl_to as CHAR) as cl_to_link,
REPLACE(cast(oo_categorylinks.cl_to as CHAR), '_',' ') as cl_to
FROM oo_searchcache
$innerjoin
left join oo_categorylinks on UPPER(REPLACE(oo_searchcache.page_title,'_',' ')) = UPPER(cast(oo_categorylinks.cl_sortkey as CHAR))

$where  $sortby_
";

$paginationlink = "/custom/php/pagination/getresult.php?page=";
$pagination_setting = $_GET["pagination_setting"];

$page = 1;
if(!empty($_GET["page"])) {
$page = $_GET["page"];
}

$start = ($page-1)*$perPage->perpage;
if($start < 0) $start = 0;

$query =  $sql . " limit " . $start . "," . $perPage->perpage;

$faq = $db_handle->runQuery($query);

if(empty($_GET["rowcount"])) {
$_GET["rowcount"] = $db_handle->numRows($sql);
}

if($pagination_setting == "prev-next") {
	$perpageresult = $perPage->getPrevNext($_GET["rowcount"], $paginationlink,$pagination_setting);	
} else {
	$perpageresult = $perPage->getAllPageLinks($_GET["rowcount"], $paginationlink,$pagination_setting);	
}



$output =  '<table class="searchone">
<tr>
     <th>Page Title</th>
     <th>Created </th>
     <th>Created By</th>
     <th>Last Modified</th>
     <th>Modified By</th>
     <th>Categories</th>
     <th>Type</th>
</tr>

';

$arrayResult = array ();
foreach($faq as $k=>$v) {

        if(isset($arrayResult[ $faq[$k]["page_title"] ])){
              $arrayResult[ $faq[$k]["page_title"]]['cl_to'] .= ","."<a target='_blank' href='/index.php/Category:".$v ["cl_to_link"]."'>".$v ["cl_to"]."</a>";
        } else {
              $arrayResult[ $faq[$k]["page_title"]] = $v;
              $arrayResult[ $faq[$k]["page_title"]]['page_title'] = "<a target='_blank'  href='/index.php/".convertNameSpace_link($v["page_namespace"]).$v ["page_title_link"]."'>".$v ["page_title"]."</a>";
              $arrayResult[ $faq[$k]["page_title"]]['cl_to'] = "<a target='_blank' aria-label='Category' href='/index.php/Category:".$v ["cl_to_link"]."'>".$v ["cl_to"]."</a>";
              $arrayResult[ $faq[$k]["page_title"]]['cl_to_link'] = '';
        }#endif

}#end foreach


 /*
print "<pre>";
print_r($faq);
print "</pre>";
*/


foreach($arrayResult as $k=>$v) {
 $output .= '<tr>';
 $output .= '<td>' . $arrayResult[$k]["page_title"] . '</td>';
 $output .= '<td>' .   getDate_( strtotime($arrayResult[$k]["rev_timestamp_created"])). '</td>';
 $output .= '<td>' . $arrayResult[$k]["created_user_text"] . '</td>';
 $output .= '<td>' .   getDate_( strtotime($arrayResult[$k]["rev_timestamp"])). '</td>';
 $output .= '<td>' . $arrayResult[$k]["rev_user_text"] . '</td>';
 $output .= '<td>' . $arrayResult[$k]["cl_to"] . '</td>';
 $output .= '<td>' . convertNameSpace($arrayResult[$k]["page_namespace"]) . '</td>';

 $output .= '</tr>';
}
$output .= '</table>';
if(!empty($perpageresult)) {
      $output .= '<div id="pagination">' . $perpageresult . '</div>';
}



if(sizeof($arrayResult)<=0){
  $output = "<div>There were no results matching the query. Use following links to create new pages : </div>";
  foreach(explode(" ",trim($_GET ['custom_search_text_all'])) as $i=>$v){
          $output .= '<div><a target="_blank" href="/index.php?title='.$v.'&amp;action=edit&amp;redlink=1" class="new" title="'.$v.' (page does not exist)">'.$v.'</a></div>';
  }#end foreach
  foreach(explode(" ",trim($_GET ['custom_search_text_exact'])) as $i=>$v){
          $output .= '<div><a target="_blank" href="/index.php?title='.$v.'&amp;action=edit&amp;redlink=1" class="new" title="'.$v.' (page does not exist)">'.$v.'</a></div>';
  }#end foreach
  foreach(explode(" ",trim($_GET ['custom_search_text_any'])) as $i=>$v){
          $output .= '<div><a target="_blank" href="/index.php?title='.$v.'&amp;action=edit&amp;redlink=1" class="new" title="'.$v.' (page does not exist)">'.$v.'</a></div>';
  }#end foreach
}#end if

print $output;

function getDate_ ($strtotime_){
   return date('d-M-Y H:i',strtotime('+2 hours',$strtotime_));
}

function convertNameSpace_link ($id){
       $return = '';
       switch(trim($id)){
            case 0:$return = '';break;
            case 2:$return = 'User:';break;
            case 4:$return = 'Project:';break;
            case 6:$return = 'File:';break;
            case 8:$return = 'MediaWiki:';break;
            case 10:$return = 'Template:';break;
            case 12:$return = 'Help:';break;
            case 14:$return = 'Category:';break;
       }
       return $return;
}
function convertNameSpace ($id){
       $return = '';
       switch(trim($id)){
            case 0:$return = 'Article';break;
            case 2:$return = 'User Page';break;
            case 4:$return = 'Project';break;
            case 6:$return = 'File';break;
            case 8:$return = 'MediaWiki';break;
            case 10:$return = 'Template';break;
            case 12:$return = 'Help';break;
            case 14:$return = 'Category';break;
       }
       return $return;
}
?>
