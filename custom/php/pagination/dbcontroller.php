<?php
class DBController {
	private $host = "localhost";
	private $user = "";
	private $password = "";
	private $database = "";
	private $conn;
	
	function __construct() {
                $this->getDB() ;
		$this->conn = $this->connectDB();
	}
	
	function connectDB() {
		$conn = mysqli_connect($this->host,$this->user,$this->password,$this->database);
		return $conn;
	}
	
	function runQuery($query) {
		$result = mysqli_query($this->conn,$query);
		while($row=mysqli_fetch_assoc($result)) {
			$resultset[] = $row;
		}
		if(!empty($resultset))
			return $resultset;
	}

	function numRows($query) {
		$result  = mysqli_query($this->conn,$query);
		$rowcount = mysqli_num_rows($result);
		return $rowcount;	
	}
        function getDB (){
            #Read config file
            $file  = file_get_contents ("../../../LocalSettings.php");
            $file = str_replace("<?php","",$file);
            $file = str_replace("?>","",$file);
            $file = str_replace("\n","",$file);
            $file = str_replace("\r","",$file);

            $db = array ('wgDBname','wgDBuser','wgDBpassword');
            preg_match('/wgDBname(.*?);/i', $file, $db ['wgDBname']);
            preg_match('/wgDBuser(.*?);/i', $file, $db ['wgDBuser']);
            preg_match('/wgDBpassword(.*?);/i', $file, $db ['wgDBpassword']);
        
            foreach ($db as $i=>$v){
                if(!is_array($v)){
                    continue;
                }
                foreach ($v as $ii=>$vv){
                   $db[$i][$ii] = str_replace('"',"",$db[$i][$ii]); 
                   $db[$i][$ii] = str_replace('=',"",$db[$i][$ii]);
                   $db[$i][$ii] = trim(str_replace('=',"",$db[$i][$ii]));
                }
            }//end foreach
            
            $this->user = $db ['wgDBuser'][1];
            $this->password = $db ['wgDBpassword'][1];
            $this->database = $db ['wgDBname'][1];
        
        }//end function
}
?>