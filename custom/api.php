<?php
#error reporting
ini_set("display_errors", "0");
error_reporting(0);

$db = getDB ();

// Create connection
$conn = new mysqli('localhost', $db['user'], $db['pass'], $db['db']);
// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}

    $_POST = array_map('mysql_real_escape_string',$_POST);
    $_GET = array_map('mysql_real_escape_string',$_GET);

 /*

 ---------------------------
Get all namespaces

SELECT DISTINCT page_namespace, count(page_id) FROM `oo_page` group by page_namespace
https://www.mediawiki.org/wiki/Manual:Namespace



 ---------------------------
 
 INSERT INTO oo_searchcache (page_title,rev_timestamp,rev_user_text) VALUES (
    (SELECT cast(page_title as CHAR) FROM oo_page where page_id = NEW.rev_page  LIMIT 1), 
    NEW.rev_timestamp,
    NEW.rev_user_text
)


   SELECT cast(cl_to as CHAR),cast(cl_sortkey as CHAR),cast(cl_type as CHAR)  FROM `oo_categorylinks` WHERE cast(cl_sortkey as CHAR) like '%SGCertifiedPlatformDeveloperI%'


   For search in particular page :::
   --->Category
   SELECT `cat_id`,cast(`cat_title` as CHAR) FROM `oo_category` ORDER BY `oo_category`.`cat_id` ASC

   ---> Category, Page Link
               INPUT : category name (with underscore etc.)
               OUTPUT : cl_sortkey and cl_type (page or file)
   SELECT cast(cl_to as CHAR),cast(cl_sortkey as CHAR),cast(cl_type as CHAR)  FROM `oo_categorylinks` WHERE cl_sortkey like '%Cobourg%'
   SELECT cast(cl_to as CHAR),cast(cl_sortkey as CHAR),cast(cl_type as CHAR),cl_timestamp FROM `oo_categorylinks` ORDER by   `cl_timestamp` DESC

   --> Search in page title. Page Titles are Stored :
   select `page_id`, cast(page_title as CHAR) as page_title,cast(page_touched as CHAR)
   ,cast(page_content_model as CHAR) from oo_page where cast(page_title as CHAR) like '%brampton%'

   --> Search latest revision
               INPUT : rev_page (From above query)
               OUTPUT : rev_text_id
   SELECT `rev_id`,`rev_text_id`, cast(`rev_comment` as CHAR), `rev_user`,cast(`rev_user_text` as CHAR) FROM `oo_revision`
    WHERE `rev_page` = 294 ORDER BY `oo_revision`.`rev_id` DESC LIMIT 1

   --> Get Actual Text of the page. Actual Text is stored
               INPUT : rev_text_id (From above query)
               OUTPUT : old_text
   select old_id,cast(old_text as CHAR) from oo_text where `old_id` = 20780 or `old_id` = 20245

   ---------------------------
   
   oo_categorylinks
   oo_page
   oo_revision
   oo_text




 */

if($_GET["action"] == "getallcategories")
{
       /*
       select DISTINCT cat_id, cast(`cat_title` as CHAR) as cat_title,`cat_subcats` ,`cat_pages`,`cat_files` from oo_category
        WHERE oo_category.cat_pages>0 ORDER BY cat_title  ASC
        */

        $sql = "select DISTINCT   REPLACE(cast(`cat_title` as CHAR),'_',' ') as cat_title, cast(`cat_title` as CHAR) as cat_id  from oo_category
        WHERE oo_category.cat_pages>0 ORDER BY cat_title  ASC ";
        $result = $conn->query($sql);

        $recordCount = 0;
        $rows = array();
        if ($result->num_rows > 0) {
            // output data of each row
            while($row = $result->fetch_assoc()) { 
                #$row ['cat_title'] = str_replace ( '_', ' ', $row ['cat_title']);
                $rows[] = $row;
                $recordCount++;
            }#end while
        }    
        
        //Return result to jTable
        $jTableResult = array();
        $jTableResult['Result'] = "OK";
        $jTableResult['TotalRecordCount'] = $recordCount;
        $jTableResult['data'] = $rows;
        print json_encode($jTableResult);

} else if($_GET["action"] == "getallpages") {

        $sql = "select DISTINCT  page_id,REPLACE(cast(`page_title` as CHAR),'_',' ') as page_title  from oo_page ORDER BY cast(`page_title` as CHAR)  ASC ";
        $result = $conn->query($sql);

        $recordCount = 0;
        $rows = array();
        if ($result->num_rows > 0) {
            while($row = $result->fetch_assoc()) {
                array_push($rows , array ('DisplayText'=>$row ['page_title'] , 'Value'=> $row ['page_id']));
            }#end while
        }    
        
        //Return result to jTable
        $jTableResult = array();
        $jTableResult['Result'] = "OK";
        $jTableResult['Options'] = $rows;
        print json_encode($jTableResult); 

} else if($_GET["action"] == "getallusers") {

        $sql = "select DISTINCT  user_id,cast(`user_name` as CHAR) as user_name  from oo_user ORDER BY  cast(`user_name` as CHAR)  ASC ";
        $result = $conn->query($sql);

        $recordCount = 0;
        $rows = array();
        array_push($rows , array ('DisplayText'=>'0 - Unregistered User', 'Value'=> 0));
        if ($result->num_rows > 0) {
            while($row = $result->fetch_assoc()) {
                array_push($rows , array ('DisplayText'=>$row ['user_name'] , 'Value'=> $row ['user_id']));
            }#end while
        }    
        
        //Return result to jTable
        $jTableResult = array();
        $jTableResult['Result'] = "OK";
        $jTableResult['Options'] = $rows;
        print json_encode($jTableResult);
        
} else if($_GET["action"] == "getallgroups") {

        $sql = "select DISTINCT   ug_group,cast(`ug_group` as CHAR) as x_ug_group  from oo_user_groups ORDER BY  cast(`ug_group` as CHAR)  ASC ";
        $result = $conn->query($sql);

        $recordCount = 0;
        $rows = array();
        if ($result->num_rows > 0) {
            while($row = $result->fetch_assoc()) {
                array_push($rows , array ('DisplayText'=>$row ['x_ug_group'], 'Value'=> $row ['ug_group']));
            }#end while
        }    
        
        //Return result to jTable
        $jTableResult = array();
        $jTableResult['Result'] = "OK";
        $jTableResult['Options'] = $rows;
        print json_encode($jTableResult);
        
} else if($_GET["action"] == "getallcats") {

        $sql = "select DISTINCT   REPLACE(cast(`cat_title` as CHAR),'_',' ') as cat_title, cast(`cat_title` as CHAR) as cat_id  from oo_category
        WHERE oo_category.cat_pages>0 ORDER BY cat_title  ASC ";
        $result = $conn->query($sql);

        $recordCount = 0;
        $rows = array();
        if ($result->num_rows > 0) {
            while($row = $result->fetch_assoc()) {
                array_push($rows , array ('DisplayText'=>$row ['cat_title'] , 'Value'=> $row ['cat_id']));
            }#end while
        }    

        //Return result to jTable
        $jTableResult = array();
        $jTableResult['Result'] = "OK";
        $jTableResult['Options'] = $rows;
        print json_encode($jTableResult);

} else if($_GET["action"] == "restrict_user_cats_list") {
		$sql = "SELECT * FROM oo_acl_restrict_user_cats;";
		$result = $conn->query($sql);

		//Add all records to an array
		$rows = array();
                if ($result->num_rows > 0) {
                    // output data of each row
                    while($row = $result->fetch_assoc()) { 
                        $rows[] = $row;
                    }#end while
                }

		//Return result to jTable
		$jTableResult = array();
		$jTableResult['Result'] = "OK";
		$jTableResult['Records'] = $rows;
		print json_encode($jTableResult);


}	else if($_GET["action"] == "restrict_user_cats_create")
	{
		//Insert record into database
		$sql =  "INSERT INTO oo_acl_restrict_user_cats(user_id, cat_id,restrict_edit,restrict_view )
                VALUES('" . $_POST["user_id"] . "', '" . $_POST["cat_id"] . "', " . $_POST["restrict_edit"] .", " . $_POST["restrict_view"]. ");";
		$result = $conn->query($sql);
		
		$sql = "SELECT * FROM oo_acl_restrict_user_cats WHERE id = LAST_INSERT_ID();";
		$result = $conn->query($sql); 

		//Add all records to an array
		$rows = array();
                if ($result->num_rows > 0) {
                    // output data of each row
                    while($row = $result->fetch_assoc()) { 
                        $rows = $row;
                    }#end while
                }

		//Return result to jTable
		$jTableResult = array();
		$jTableResult['Result'] = "OK";
		$jTableResult['Record'] = $rows;
		print json_encode($jTableResult);
	}
	//Updating a record (updateAction)
	else if($_GET["action"] == "restrict_user_cats_update")
	{
		//Update record in database
		$sql = "UPDATE oo_acl_restrict_user_cats SET user_id = '" . $_POST["user_id"] 
                . "', restrict_edit = '" . $_POST["restrict_edit"]
                . "', restrict_view = '" . $_POST["restrict_view"]
                . "', cat_id = '" . $_POST["cat_id"] . "'
                WHERE id = " . $_POST["id"] . ";";
		$result = $conn->query($sql);

		//Return result to jTable
		$jTableResult = array();
		$jTableResult['Result'] = "OK";
		print json_encode($jTableResult);
	}
	//Deleting a record (deleteAction)
	else if($_GET["action"] == "restrict_user_cats_delete")
	{
		//Delete from database
		$sql = "DELETE FROM oo_acl_restrict_user_cats WHERE id = " . $_POST["id"] . ";";
		$result = $conn->query($sql);

		//Return result to jTable
		$jTableResult = array();
		$jTableResult['Result'] = "OK";
		print json_encode($jTableResult);

} else if($_GET["action"] == "restrict_group_cats_list") {
		$sql = "SELECT * FROM oo_acl_restrict_group_cats;";
		$result = $conn->query($sql);

		//Add all records to an array
		$rows = array();
                if ($result->num_rows > 0) {
                    // output data of each row
                    while($row = $result->fetch_assoc()) { 
                        $rows[] = $row;
                    }#end while
                }

		//Return result to jTable
		$jTableResult = array();
		$jTableResult['Result'] = "OK";
		$jTableResult['Records'] = $rows;
		print json_encode($jTableResult);


}	else if($_GET["action"] == "restrict_group_cats_create")
	{
		//Insert record into database
		$sql =  "INSERT INTO oo_acl_restrict_group_cats(group_id, cat_id,restrict_edit,restrict_view )
                VALUES('" . $_POST["group_id"] . "', '" . $_POST["cat_id"] . "', " . $_POST["restrict_edit"] .", " . $_POST["restrict_view"]. ");";
		$result = $conn->query($sql);
		
		$sql = "SELECT * FROM oo_acl_restrict_group_cats WHERE id = LAST_INSERT_ID();";
		$result = $conn->query($sql); 

		//Add all records to an array
		$rows = array();
                if ($result->num_rows > 0) {
                    // output data of each row
                    while($row = $result->fetch_assoc()) { 
                        $rows = $row;
                    }#end while
                }

		//Return result to jTable
		$jTableResult = array();
		$jTableResult['Result'] = "OK";
		$jTableResult['Record'] = $rows;
		print json_encode($jTableResult);
	}
	//Updating a record (updateAction)
	else if($_GET["action"] == "restrict_group_cats_update")
	{
		//Update record in database
		$sql = "UPDATE oo_acl_restrict_group_cats SET group_id = '" . $_POST["group_id"]
                . "', restrict_edit = '" . $_POST["restrict_edit"]
                . "', restrict_view = '" . $_POST["restrict_view"]
                . "', cat_id = '" . $_POST["cat_id"] . "'
                WHERE id = " . $_POST["id"] . ";";
		$result = $conn->query($sql);

		//Return result to jTable
		$jTableResult = array();
		$jTableResult['Result'] = "OK";
		print json_encode($jTableResult);
	}
	//Deleting a record (deleteAction)
	else if($_GET["action"] == "restrict_group_cats_delete")
	{
		//Delete from database
		$sql = "DELETE FROM oo_acl_restrict_group_cats WHERE id = " . $_POST["id"] . ";";
		$result = $conn->query($sql);

		//Return result to jTable
		$jTableResult = array();
		$jTableResult['Result'] = "OK";
		print json_encode($jTableResult);


} else if($_GET["action"] == "restrict_user_page_list") {
		$sql = "SELECT * FROM oo_acl_restrict_user_page;";
		$result = $conn->query($sql);

		//Add all records to an array
		$rows = array();
                if ($result->num_rows > 0) {
                    // output data of each row
                    while($row = $result->fetch_assoc()) { 
                        $rows[] = $row;
                    }#end while
                }

		//Return result to jTable
		$jTableResult = array();
		$jTableResult['Result'] = "OK";
		$jTableResult['Records'] = $rows;
		print json_encode($jTableResult);


}	else if($_GET["action"] == "restrict_user_page_create")
	{
		//Insert record into database
		$sql =  "INSERT INTO oo_acl_restrict_user_page(user_id, page_id,restrict_edit,restrict_view )
                VALUES('" . $_POST["user_id"] . "', " . $_POST["page_id"] . ", " . $_POST["restrict_edit"] .", " . $_POST["restrict_view"]. ");";
		$result = $conn->query($sql);
		
		$sql = "SELECT * FROM oo_acl_restrict_user_page WHERE id = LAST_INSERT_ID();";
		$result = $conn->query($sql); 

		//Add all records to an array
		$rows = array();
                if ($result->num_rows > 0) {
                    // output data of each row
                    while($row = $result->fetch_assoc()) { 
                        $rows = $row;
                    }#end while
                }

		//Return result to jTable
		$jTableResult = array();
		$jTableResult['Result'] = "OK";
		$jTableResult['Record'] = $rows;
		print json_encode($jTableResult);
	}
	//Updating a record (updateAction)
	else if($_GET["action"] == "restrict_user_page_update")
	{
		//Update record in database
		$sql = "UPDATE oo_acl_restrict_user_page SET user_id = '" . $_POST["user_id"] 
                . "', restrict_edit = '" . $_POST["restrict_edit"]
                . "', restrict_view = '" . $_POST["restrict_view"]
                . "', page_id = '" . $_POST["page_id"] . "'
                WHERE id = " . $_POST["id"] . ";";
		$result = $conn->query($sql);

		//Return result to jTable
		$jTableResult = array();
		$jTableResult['Result'] = "OK";
		print json_encode($jTableResult);
	}
	//Deleting a record (deleteAction)
	else if($_GET["action"] == "restrict_user_page_delete")
	{
		//Delete from database
		$sql = "DELETE FROM oo_acl_restrict_user_page WHERE id = " . $_POST["id"] . ";";
		$result = $conn->query($sql);

		//Return result to jTable
		$jTableResult = array();
		$jTableResult['Result'] = "OK";
		print json_encode($jTableResult);

} else if($_GET["action"] == "restrict_group_page_list") {
		$sql = "SELECT * FROM oo_acl_restrict_group_page;";
		$result = $conn->query($sql);

		//Add all records to an array
		$rows = array();
                if ($result->num_rows > 0) {
                    // output data of each row
                    while($row = $result->fetch_assoc()) {
                        $rows[] = $row;
                    }#end while
                }

		//Return result to jTable
		$jTableResult = array();
		$jTableResult['Result'] = "OK";
		$jTableResult['Records'] = $rows;
		print json_encode($jTableResult);


}	else if($_GET["action"] == "restrict_group_page_create")
	{
		//Insert record into database
		$sql =  "INSERT INTO oo_acl_restrict_group_page(group_id, page_id,restrict_edit,restrict_view ) 
                VALUES('" . $_POST["group_id"] . "', " . $_POST["page_id"] . ", " . $_POST["restrict_edit"] . ", " . $_POST["restrict_view"] . ");";
		$result = $conn->query($sql);
		
		$sql = "SELECT * FROM oo_acl_restrict_group_page WHERE id = LAST_INSERT_ID();";
		$result = $conn->query($sql); 

		//Add all records to an array
		$rows = array();
                if ($result->num_rows > 0) {
                    // output data of each row
                    while($row = $result->fetch_assoc()) { 
                        $rows = $row;
                    }#end while
                }

		//Return result to jTable
		$jTableResult = array();
		$jTableResult['Result'] = "OK";
		$jTableResult['Record'] = $rows;
		print json_encode($jTableResult);
	}
	//Updating a record (updateAction)
	else if($_GET["action"] == "restrict_group_page_update")
	{
		//Update record in database
		$sql = "UPDATE oo_acl_restrict_group_page SET restrict_view = '" . $_POST["restrict_view"] . ",restrict_edit = '" . $_POST["restrict_edit"] . ",group_id = '" . $_POST["user_id"] . "', page_id = '" . $_POST["page_id"] . "'
                WHERE id = " . $_POST["id"] . ";";
		$result = $conn->query($sql);

		//Return result to jTable
		$jTableResult = array();
		$jTableResult['Result'] = "OK";
		print json_encode($jTableResult);
	}
	//Deleting a record (deleteAction)
	else if($_GET["action"] == "restrict_group_page_delete")
	{
		//Delete from database
		$sql = "DELETE FROM oo_acl_restrict_group_page WHERE id = " . $_POST["id"] . ";";
		$result = $conn->query($sql);

		//Return result to jTable
		$jTableResult = array();
		$jTableResult['Result'] = "OK";
		print json_encode($jTableResult);
	}


$conn->close();
exit(1); 
 
// Functions
function getDB (){
    #Read config file
    $file  = file_get_contents ("../LocalSettings.php");
    $file = str_replace("<?php","",$file);
    $file = str_replace("?>","",$file);
    $file = str_replace("\n","",$file);
    $file = str_replace("\r","",$file);
    
    $db = array ('wgDBname','wgDBuser','wgDBpassword');
    preg_match('/wgDBname(.*?);/i', $file, $db ['wgDBname']);
    preg_match('/wgDBuser(.*?);/i', $file, $db ['wgDBuser']);
    preg_match('/wgDBpassword(.*?);/i', $file, $db ['wgDBpassword']);

    foreach ($db as $i=>$v){
        if(!is_array($v)){
            continue;
        }
        foreach ($v as $ii=>$vv){
           $db[$i][$ii] = str_replace('"',"",$db[$i][$ii]); 
           $db[$i][$ii] = str_replace('=',"",$db[$i][$ii]);
           $db[$i][$ii] = trim(str_replace('=',"",$db[$i][$ii]));
        }
    }//end foreach
    
    return array (
        'db'=> $db ['wgDBname'][1],
        'user'=> $db ['wgDBuser'][1],
        'pass'=> $db ['wgDBpassword'][1],
    );
    
}//end function
?>