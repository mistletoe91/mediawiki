<?php

ini_set("display_errors", "0");
error_reporting(0);

$db = getDB ();

// Create connection
$conn = new mysqli('localhost', $db['user'], $db['pass'], $db['db']);
// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}
  
                $return = array ('oo_acl_restrict_user_cats',
                'oo_acl_restrict_group_cats',
                'oo_acl_restrict_user_page',
                'oo_acl_restrict_group_page');

		$sql = "SELECT * FROM oo_acl_restrict_user_cats where cat_id like ;";
		$result = $conn->query($sql);

		//Add all records to an array
		$rows = array();
                if ($result->num_rows > 0) {
                    // output data of each row
                    while($row = $result->fetch_assoc()) { 
                        $rows[] = $row;
                    }#end while
                }
                $return ['oo_acl_restrict_user_cats'] = $rows;

		$sql = "SELECT * FROM oo_acl_restrict_group_cats;";
		$result = $conn->query($sql);

		//Add all records to an array
		$rows = array();
                if ($result->num_rows > 0) {
                    // output data of each row
                    while($row = $result->fetch_assoc()) { 
                        $rows[] = $row;
                    }#end while
                }
                $return ['oo_acl_restrict_group_cats'] = $rows;


		$sql = "SELECT * FROM oo_acl_restrict_user_page;";
		$result = $conn->query($sql);

		//Add all records to an array
		$rows = array();
                if ($result->num_rows > 0) {
                    // output data of each row
                    while($row = $result->fetch_assoc()) { 
                        $rows[] = $row;
                    }#end while
                }
                $return ['oo_acl_restrict_user_page'] = $rows;
                

		$sql = "SELECT * FROM oo_acl_restrict_group_page;";
		$result = $conn->query($sql);

		//Add all records to an array
		$rows = array();
                if ($result->num_rows > 0) {
                    // output data of each row
                    while($row = $result->fetch_assoc()) {
                        $rows[] = $row;
                    }#end while
                }
                $return ['oo_acl_restrict_group_page'] = $rows;
                


$conn->close();
return $return;

// Functions
function getDB (){
    #Read config file
    $file  = file_get_contents ("../LocalSettings.php");
    $file = str_replace("<?php","",$file);
    $file = str_replace("?>","",$file);
    $file = str_replace("\n","",$file);
    $file = str_replace("\r","",$file);
    
    $db = array ('wgDBname','wgDBuser','wgDBpassword');
    preg_match('/wgDBname(.*?);/i', $file, $db ['wgDBname']);
    preg_match('/wgDBuser(.*?);/i', $file, $db ['wgDBuser']);
    preg_match('/wgDBpassword(.*?);/i', $file, $db ['wgDBpassword']);

    foreach ($db as $i=>$v){
        if(!is_array($v)){
            continue;
        }
        foreach ($v as $ii=>$vv){
           $db[$i][$ii] = str_replace('"',"",$db[$i][$ii]); 
           $db[$i][$ii] = str_replace('=',"",$db[$i][$ii]);
           $db[$i][$ii] = trim(str_replace('=',"",$db[$i][$ii]));
        }
    }//end foreach
    
    return array (
        'db'=> $db ['wgDBname'][1],
        'user'=> $db ['wgDBuser'][1],
        'pass'=> $db ['wgDBpassword'][1],
    );
    
}//end function
?>