DROP TRIGGER IF EXISTS `On Insert - Put Record in SearchCache and SearchCache_Blob`;
DROP TABLE IF EXISTS `oo_searchcache`;
DROP TABLE IF EXISTS `oo_searchcache_blob`;



--
-- Triggers `oo_revision`
--
DELIMITER $$
CREATE TRIGGER `On Insert - Put Record in SearchCache and SearchCache_Blob` AFTER INSERT ON `oo_revision` FOR EACH ROW BEGIN
    DECLARE x_pagetitle varchar(255);
    DECLARE x_created_user_text varchar(255);
    DECLARE x_rev_timestamp_created varchar(255);
    DECLARE x_searchcache_id  INT(11);
    DECLARE x_id INT(11); 
    SET x_id = 0;

    SET x_pagetitle =  (SELECT cast(page_title as CHAR) FROM oo_page where page_id = NEW.rev_page  LIMIT 1);
    SET x_id =  (SELECT id FROM oo_searchcache where page_title like x_pagetitle LIMIT 1);

    SET x_created_user_text = (SELECT rev_user_text from oo_revision where rev_id IN
    ( SELECT min(rev_id) FROM `oo_revision` WHERE rev_page = NEW.rev_page  group by rev_page ));
    SET x_rev_timestamp_created = (SELECT rev_timestamp from oo_revision where rev_id IN
    ( SELECT min(rev_id) FROM `oo_revision` WHERE rev_page = NEW.rev_page  group by rev_page ));

    IF(x_id > 0) THEN
        
       UPDATE oo_searchcache SET rev_timestamp = NEW.rev_timestamp,rev_user_text = NEW.rev_user_text where oo_searchcache.id= x_id;
       UPDATE oo_searchcache_blob SET old_text = (SELECT cast(old_text as CHAR) FROM `oo_text` WHERE old_id = NEW.rev_text_id)  WHERE  oo_searchcache_blob.oo_searchcache_id =x_id;
        
    ELSE
         
        INSERT INTO oo_searchcache (page_title,rev_timestamp,rev_user_text,created_user_text) VALUES (
             x_pagetitle,
             NEW.rev_timestamp,
             NEW.rev_user_text,
             x_created_user_text
        );
        SET x_searchcache_id = LAST_INSERT_ID ();
        INSERT INTO oo_searchcache_blob (	oo_searchcache_id, old_text )
        SELECT DISTINCT x_searchcache_id, cast(old_text as CHAR) FROM `oo_text` WHERE old_id = NEW.rev_text_id;
         
    END IF;
END
$$
DELIMITER ; 


CREATE TABLE `oo_searchcache` (
  `id` int(11) NOT NULL,
  `page_title` varchar(255) DEFAULT NULL,
  `rev_timestamp` varchar(255) DEFAULT NULL,
  `rev_timestamp_created` varchar(255) DEFAULT NULL,
  `rev_user_text` varchar(255) DEFAULT NULL,
  `created_user_text` varchar(255) DEFAULT NULL,
  `page_namespace` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
 

CREATE TABLE `oo_searchcache_blob` (
  `id` int(11) NOT NULL,
  `oo_searchcache_id` int(11) NOT NULL,
  `old_text` mediumtext NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8; 


--
-- Indexes for table `oo_searchcache`
--
ALTER TABLE `oo_searchcache`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oo_searchcache_blob`
--
ALTER TABLE `oo_searchcache_blob`
  ADD PRIMARY KEY (`id`);
 
--
-- AUTO_INCREMENT for table `oo_searchcache`
--
ALTER TABLE `oo_searchcache`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21114;
--
-- AUTO_INCREMENT for table `oo_searchcache_blob`
--
ALTER TABLE `oo_searchcache_blob`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16827; 