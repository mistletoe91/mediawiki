
-- --------------------------------------------------------

--
-- Table structure for table `oo_acl_restrict_group_cats`
--

DROP TABLE IF EXISTS `oo_acl_restrict_group_cats`;
CREATE TABLE `oo_acl_restrict_group_cats` (
  `id` int(11) NOT NULL,
  `group_id` varchar(255) DEFAULT NULL,
  `cat_id` varchar(255) DEFAULT NULL,
  `restrict_edit` tinyint(1) DEFAULT '1',
  `restrict_view` tinyint(1) DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oo_acl_restrict_group_page`
--

DROP TABLE IF EXISTS `oo_acl_restrict_group_page`;
CREATE TABLE `oo_acl_restrict_group_page` (
  `id` int(11) NOT NULL,
  `group_id` varchar(255) DEFAULT NULL,
  `page_id` varchar(255) DEFAULT NULL,
  `restrict_edit` tinyint(1) DEFAULT '1',
  `restrict_view` tinyint(1) DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oo_acl_restrict_group_page`
--


-- --------------------------------------------------------

--
-- Table structure for table `oo_acl_restrict_user_cats`
--

DROP TABLE IF EXISTS `oo_acl_restrict_user_cats`;
CREATE TABLE `oo_acl_restrict_user_cats` (
  `id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `cat_id` varchar(255) DEFAULT NULL,
  `restrict_edit` tinyint(1) DEFAULT '1',
  `restrict_view` tinyint(1) DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oo_acl_restrict_user_cats`
--

-- --------------------------------------------------------

--
-- Table structure for table `oo_acl_restrict_user_page`
--

DROP TABLE IF EXISTS `oo_acl_restrict_user_page`;
CREATE TABLE `oo_acl_restrict_user_page` (
  `id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `page_id` varchar(255) DEFAULT NULL,
  `restrict_edit` tinyint(1) DEFAULT '1',
  `restrict_view` tinyint(1) DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oo_acl_restrict_user_page`
--

INSERT INTO `oo_acl_restrict_user_page` (`id`, `user_id`, `page_id`, `restrict_edit`, `restrict_view`) VALUES
(3, 0, '3642', 1, 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `oo_acl_restrict_group_cats`
--
ALTER TABLE `oo_acl_restrict_group_cats`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oo_acl_restrict_group_page`
--
ALTER TABLE `oo_acl_restrict_group_page`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oo_acl_restrict_user_cats`
--
ALTER TABLE `oo_acl_restrict_user_cats`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oo_acl_restrict_user_page`
--
ALTER TABLE `oo_acl_restrict_user_page`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `oo_acl_restrict_group_cats`
--
ALTER TABLE `oo_acl_restrict_group_cats`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `oo_acl_restrict_group_page`
--
ALTER TABLE `oo_acl_restrict_group_page`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `oo_acl_restrict_user_cats`
--
ALTER TABLE `oo_acl_restrict_user_cats`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `oo_acl_restrict_user_page`
--
ALTER TABLE `oo_acl_restrict_user_page`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5; 