$(document).ready(function() {
  
    
$('#link_advance').click( function(e) {e.preventDefault(); 
       //$('#link_advance').fadeOut ();   
       $("#link_advance").css({ display: "none" });   
       $(".div_advance").css({ display: "block" });                                        
return false; } ); 
    
$('#btnreset').click( function(e) {e.preventDefault(); 
      $('#custom_cat').val(0);
      $('#custom_search_for').val(999);
      $('#custom_search_in').val(1);
      
      $('#custom_sort_by').val(0);
      //$('#custom_sort_by option:first-child').attr("selected", "selected");
                                   
      $('#custom_sort_order').val('DESC');
      $('#custom_sort_perpage').val('999999');                              
                                   

} );       
    
     
    
    
    

if($("#custom_search_text_any").val().length  > 0){ 
          getresult ('/custom/php/pagination/getresult.php');
}//endif

$("#form_customsearch").submit(function(event) {

      /* stop form from submitting normally */
      event.preventDefault();

      /* get the action attribute from the <form action=""> element */
      var $form = $( this ),
          url = $form.attr( 'action' );
      
      getresult (url);

});

/*
$( "#btnsearch" ).click(function() {
  getresult("/custom/php/pagination/getresult.php");
});
*/

$.getJSON("/custom/api.php?action=getallcategories", null, function(data) {
    $("#custom_cat option").remove(); // Remove all <option> child tags.
    $("#custom_cat").append($("<option></option>").text("All").val(0));

    $.each(data.data, function(index, item) { // Iterates through a collection
        $("#custom_cat").append( // Append an object to the inside of the select box
            $("<option></option>") // Yes you can do this.
                .text(item.cat_title) // + ' Pages('+item.cat_pages+') Files('+item.cat_files+')')
                .val(item.cat_id)
        );
    });
});
    /*
    $('#customsearch_results').DataTable( {
        "ajax": "/custom/api.php?action=getallcategories",
        "columns": [
            { "data": "cat_id" },
            { "data": "cat_title" }
        ]
    } );
    */

} );

function getresult(url) {

        $('div#pagination-result').html('<div id="overlay" style="display:block;"><img src="/custom/images/loading.gif" class="loading_circle" alt="loading" /></div>');
	$.ajax({
		url: url,
		type: "GET",
		data:  {
                   custom_search_text_all:$("#custom_search_text_all").val(),
                   custom_search_text_exact:$("#custom_search_text_exact").val(),
                   custom_search_text_any:$("#custom_search_text_any").val(),
                   custom_search_text_none:$("#custom_search_text_none").val(),
                   custom_cat:$("#custom_cat").val(),
                   custom_search_for:$("#custom_search_for").val(),
                   custom_search_in:$("#custom_search_in").val(),
                   custom_sort_by:$("#custom_sort_by").val(),
                   custom_search_case:$("#custom_search_case").val(),
                   custom_sort_order:$("#custom_sort_order").val(),
                   custom_sort_perpage:$("#custom_sort_perpage").val(),
                   rowcount:$("#rowcount").val(),
                   "pagination_setting":$("#pagination-setting").val()
                },
		beforeSend: function(){
                  //$("#overlay").css("display", "block");
                },
		success: function(data){
		           $("#pagination-result").html('<div id="overlay" style="display:none;"><img src="/custom/images/loading.gif" class="loading_circle" alt="loading" /></div>'+data);
                           //setInterval(function() {$("#overlay").css("display", "none"); },500);
		},
		error: function() 
		{} 	        
   });
}
function changePagination(option) {
	if(option!= "") {
		getresult("/custom/php/pagination/getresult.php");
	}
}