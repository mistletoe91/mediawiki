
function copyToTextArea (elemid){
    var elem=document.getElementById('ele'+elemid);
    var ele_linktext=document.getElementById('ele_linktext'+elemid);
    var wpTextbox1=document.getElementById('wpTextbox1');
    var newvalue = '<a href="'+elem.value+'">'+ele_linktext.value+'</a>';
    var result = '<p>'+newvalue+'</p>';   //wpTextbox1.value+

    //CKEDITOR.instances['wpTextbox1'].setData(result);
    var existingVal = $('#wpTextbox1').val();
    $('#wpTextbox1').val(existingVal+'['+elem.value+' '+ele_linktext.value+']');
 
}


function copyToClipboard(elemid) {
    var elem=document.getElementById('ele'+elemid);
    elem.select();

	  // create hidden text element, if it doesn't already exist
    var targetId = "_hiddenCopyText_";
    var isInput = elem.tagName === "INPUT" || elem.tagName === "TEXTAREA";
    var origSelectionStart, origSelectionEnd;
    if (isInput) {
        // can just use the original source element for the selection and copy
        target = elem;
        origSelectionStart = elem.selectionStart;
        origSelectionEnd = elem.selectionEnd;
    } else {
        // must use a temporary form element for the selection and copy
        target = document.getElementById(targetId);
        if (!target) {
            var target = document.createElement("textarea");
            target.style.position = "absolute";
            target.style.left = "-9999px";
            target.style.top = "0";
            target.id = targetId;
            document.body.appendChild(target);
        }
        target.textContent = elem.textContent;
    }
    // select the content
    var currentFocus = document.activeElement;
    target.focus();
    target.setSelectionRange(0, target.value.length);
    
    // copy the selection
    var succeed;
    try {
    	  succeed = document.execCommand("copy");
    } catch(e) {
        succeed = false;
    }
    // restore original focus
    if (currentFocus && typeof currentFocus.focus === "function") {
        currentFocus.focus();
    }
    
    if (isInput) {
        // restore prior selection
        elem.setSelectionRange(origSelectionStart, origSelectionEnd);
    } else {
        // clear temporary content
        target.textContent = "";
    }
    return succeed;
}
$(function () {
    $i=0;
    $('#fileupload').fileupload({
        dataType: 'json',
        done: function (e, data) {
            $.each(data.result.files, function (index, file) {
                //$('<input value="'+file.name+" <a href='#' onclick='copyToClipboard(\""+file.url+"\")'>Copy Link</a></p>").appendTo("#fileupload_custom"); 

                 var textlinkfield = '<input id="ele_linktext'+$i+'" value="'+file.name+'" style="width:100px;" />';
                 var linktxt = '<input id="ele'+$i+'" value="'+window.location.protocol + "//" + window.location.host + ""+file.url+'" style="width:300px;" />';
                 var linkedfun = "<a style='border:1px solid #ddd;padding:0px 10px;background:#ddd;' href='#' onclick='copyToTextArea(\""+$i+"\")'>Insert Link</a>";
                 $('<div style="border:1px solid #ddd;padding:10px;">Text '+textlinkfield+'&nbsp;&nbsp;Link '+linktxt+"&nbsp;&nbsp;"+linkedfun+'</div>').appendTo("#fileupload_custom");


                    document.getElementById("ele"+$i).onclick = function() {
                        this.select();
                    }
                    $i++;

            });
        }
    });
});