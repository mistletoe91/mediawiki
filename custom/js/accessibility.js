

$( document ).ready(function() {

function rgb2hex(rgb) {
     if (  rgb.search("rgb") == -1 ) {
          return rgb;
     } else {
          rgb = rgb.match(/^rgba?\((\d+),\s*(\d+),\s*(\d+)(?:,\s*(\d+))?\)$/);
          function hex(x) {
               return ("0" + parseInt(x).toString(16)).slice(-2);
          }
          return "#" + hex(rgb[1]) + hex(rgb[2]) + hex(rgb[3]); 
     }
}
    
//Search field for tabindex  
$("#searchInput").prop('disabled', true);   
//Remove All tabindex 
function makeTimeoutFunc(param) {
    return function() { 
        $('input').removeAttr("tabindex");
        $("#searchInput").prop('disabled', false);
    }
} 
setTimeout(makeTimeoutFunc(), 5000);
    
    //Show Hide Fix 
//Remove All tabindex 
function makeTimeoutFunc_showhide(param) {
    return function() { 
        
        $('#toc ul:first').each(function(){  
            if(!$(this).attr("id")){  
                     $(this).attr("id", "ctltocul"); 
            }//endif            
        });
        
        $('#toctitle .toctoggle a:first').each(function(){ 
            $(this).attr("aria-controls", "ctltocul");   
            if($(this).html().toLowerCase() == "hide"){
                $(this).attr("aria-expanded", "true");   
            } else {
                $(this).attr("aria-expanded", "false");   
            }
        });
        
    }
} 
setTimeout(makeTimeoutFunc_showhide(), 5000);    
     

//Magically add aria-label from placeholder if aria-label does not exist    
$('input').each(function(){    
     
    if(!$(this).attr("aria-label")){ 
        if($(this).attr("placeholder")){
             $(this).attr("aria-label", $(this).attr("placeholder"));
        }//endif
    }//endif
        
});
    
$('.error').each(function(){    
    if(!$(this).attr("role")){  
             $(this).attr("role", 'alert'); 
    }//endif    
});
    
$('table:not(.jtable)').each(function(){    
    
    if(!$(this).attr("role")){  
             $(this).attr("role", 'presentation'); 
    }//endif    
});  

//Fix red link spans
$('#mw-content-text span').each(function(){    
    if($(this).css("color")){  
            if(rgb2hex($(this).css("color")) == "#ff0000"){
                var newColor = "#8c001a";
                $(this).css("color",newColor);
                $(this).parent().css("color",newColor);
            }//endif
             //$(this).attr("role", 'presentation'); 
    }//endif    
});  
     
    
var $counter_ = 1;
$('.mw-history-compareselectedversions-button').each(function(){
    $(this).attr('id', 'ctlcompareselectedversions'+$counter_++);
});
    
    
    
//Image tag fix not([alt])
$('img').each(function(){
    var altTag = $(this).attr('alt');
     
    if (altTag  && altTag != '') {
        return; 
    } //endif
    
      Filename= $(this).attr('src').split('/').pop();     
      Filename = Filename.replace(/png/ig, "");
      Filename = Filename.replace(/jpg/ig, "");
      Filename = Filename.replace(/jpeg/ig, "");
      Filename = Filename.replace(/\./g, "");
      Filename = Filename.replace(/_/g, " "); 
     
    if( Filename == "fileicon-pdf"){
        //PDF
        Filename = $(this).parent("a").attr("href");
        
          Filename = Filename.split('/').pop();     
          Filename = Filename.replace(/pdf/ig, "");
          Filename = Filename.replace(/png/ig, "");
          Filename = Filename.replace(/jpg/ig, "");
          Filename = Filename.replace(/jpeg/ig, "");
          Filename = Filename.replace(/\./g, "");
          Filename = Filename.replace(/_/g, " "); 
        
        //console.log ("--->"+ Filename);
        
    } else {
        //Images
        $(this).attr("alt",Filename);     
    }
    
    
}).get()
    
    
});

