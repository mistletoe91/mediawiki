-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Oct 10, 2017 at 05:13 PM
-- Server version: 10.1.13-MariaDB
-- PHP Version: 5.6.23

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `mediawiki125`
--

-- --------------------------------------------------------

--
-- Table structure for table `oo_revision`
--


--
-- Triggers `oo_revision`
--
DROP TRIGGER IF EXISTS `On Insert - Put Record in SearchCache and SearchCache_Blob`;
DELIMITER $$
CREATE TRIGGER `On Insert - Put Record in SearchCache and SearchCache_Blob` AFTER INSERT ON `oo_revision` FOR EACH ROW BEGIN
    DECLARE x_pagetitle varchar(255);
    DECLARE x_searchcache_id  INT(11);
    DECLARE x_id INT(11); 
    SET x_id = 0;
  
    SET x_pagetitle =  (SELECT cast(page_title as CHAR) FROM oo_page where page_id = NEW.rev_page  LIMIT 1);
    SET x_id =  (SELECT id FROM oo_searchcache where page_title like x_pagetitle LIMIT 1);

    IF(x_id > 0) THEN
        
       UPDATE oo_searchcache SET rev_timestamp = NEW.rev_timestamp,rev_user_text = NEW.rev_user_text where oo_searchcache.id= x_id;
       UPDATE oo_searchcache_blob SET old_text = (SELECT cast(old_text as CHAR) FROM `oo_text` WHERE old_id = NEW.rev_text_id)  WHERE  oo_searchcache_blob.oo_searchcache_id =x_id;
        
    ELSE
         
        INSERT INTO oo_searchcache (page_title,rev_timestamp,rev_user_text) VALUES (
             x_pagetitle,
             NEW.rev_timestamp,
             NEW.rev_user_text
        );
        SET x_searchcache_id = LAST_INSERT_ID ();
        INSERT INTO oo_searchcache_blob (	oo_searchcache_id, old_text )
        SELECT DISTINCT x_searchcache_id, cast(old_text as CHAR) FROM `oo_text` WHERE old_id = NEW.rev_text_id;
         
    END IF;
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `oo_searchcache`
--

DROP TABLE IF EXISTS `oo_searchcache`;
CREATE TABLE `oo_searchcache` (
  `id` int(11) NOT NULL,
  `page_title` varchar(255) DEFAULT NULL,
  `rev_timestamp` varchar(255) DEFAULT NULL,
  `rev_user_text` varchar(255) DEFAULT NULL,
  `page_namespace` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

 
--
-- Table structure for table `oo_searchcache_blob`
--

DROP TABLE IF EXISTS `oo_searchcache_blob`;
CREATE TABLE `oo_searchcache_blob` (
  `id` int(11) NOT NULL,
  `oo_searchcache_id` int(11) NOT NULL,
  `old_text` mediumtext NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
 
-- Indexes for dumped tables
--
 

--
-- Indexes for table `oo_searchcache`
--
ALTER TABLE `oo_searchcache`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oo_searchcache_blob`
--
ALTER TABLE `oo_searchcache_blob`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `oo_searchcache`
--
ALTER TABLE `oo_searchcache`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17589;
--
-- AUTO_INCREMENT for table `oo_searchcache_blob`
--
ALTER TABLE `oo_searchcache_blob`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13250;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
