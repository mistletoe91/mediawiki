<?php

ini_set("display_errors", "1");
error_reporting(E_ALL);

$db = getDB ();

// Create connection
$conn = new mysqli('localhost', $db['user'], $db['pass'], $db['db']);
// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}

        $sql = "delete from oo_searchcache ;";
        $result = $conn->query($sql);
        $sql = "delete from oo_searchcache_blob;";
        $result = $conn->query($sql);
        
        # Insert

        $sql = "SELECT max(rev_id) as latest_rev, rev_page FROM `oo_revision` group by rev_page ORDER BY `oo_revision`.`rev_id` DESC ;";
        $result = $conn->query($sql);

        $recordCount = 0;
        $rows = array();
        if ($result->num_rows > 0) {
            while($row = $result->fetch_assoc()) {


                $latest_rev =  $row ['latest_rev'];
                $rev_page =  $row ['rev_page'];

                #
                $sql = 'select page_namespace,oo_revision.rev_id,cast(oo_page.page_title as CHAR) as page_title,cast(rev_timestamp as char) as x_timestamp
                ,cast(rev_user_text as char) as x_user,cast(old_text as char) as x_blob ,cast(old_id as char) as x_old_id,
                oo_revision.rev_page as x_rev_page
from oo_revision
 inner join oo_page on oo_revision.rev_page = oo_page.page_id
 left join oo_text on oo_revision.rev_text_id = oo_text.old_id
                where oo_revision.rev_id ='. $latest_rev;

                $result_inner = $conn->query($sql);
                while($row_ = $result_inner->fetch_assoc()) {
                         #echo "<br />".$row_['page_title'].' '.$row_['x_timestamp'].' '.$row_['x_user'];
                        # print_r($row_);
                        

                         #Get created by
                         $sql__ = "(SELECT rev_user_text as created_user_text from oo_revision where rev_id IN
                             ( SELECT min(rev_id) FROM `oo_revision` WHERE rev_page = ".$row_['x_rev_page']."  group by rev_page ))";
                         $result__ = $conn->query($sql__);
                         $created_user_text = '';
                         while($row1_ = $result__->fetch_assoc()) {
                                   $created_user_text = $row1_['created_user_text'];
                                   break;
                         }#end while
                         
                         $sql__ = "(SELECT rev_timestamp  as rev_timestamp_created from oo_revision where rev_id IN
                             ( SELECT min(rev_id) FROM `oo_revision` WHERE rev_page = ".$row_['x_rev_page']."  group by rev_page ))";
                         $result__ = $conn->query($sql__);
                         $rev_timestamp_created = '';
                         while($row1_ = $result__->fetch_assoc()) {
                                   $rev_timestamp_created = $row1_['rev_timestamp_created'];
                                   break;
                         }#end while


                         $query = "INSERT INTO `oo_searchcache`(`created_user_text`,`rev_timestamp_created`,`page_title`, `rev_timestamp`, `rev_user_text`, `page_namespace`)
                         VALUES ('".$created_user_text."','".$rev_timestamp_created."','".$row_['page_title']."','".$row_['x_timestamp']."','".$row_['x_user']."','".$row_['page_namespace']."')";
                         $oo_searchcache_query = $conn->query($query);
                         $last_id = mysqli_insert_id($conn);
                         echo "<br />$query";

                        if (mysqli_query($conn, $sql)) {

                            $query = "INSERT INTO `oo_searchcache_blob`(`oo_searchcache_id`, `old_text`)
                            VALUES ('".$last_id."','".$conn->real_escape_string($row_['x_blob'])."')";
                            echo "<br />-->". $row_['x_old_id'];
                            $oo_searchcache_blob_query = $conn->query($query);

                            $last_id = mysqli_insert_id($conn);
                            echo "<br> Inserted in blob: $last_id";
                            #echo "New record created successfully. Last inserted ID is: " . $last_id;
                        } else {
                            echo "Error: " . $sql . "<br>" . mysqli_error($conn);
                        }

                         


                }#end while
            }#end while
        }#endif


$conn->close();
exit(1);

// Functions
function getDB (){
    #Read config file
    $file  = file_get_contents ("../LocalSettings.php");
    $file = str_replace("<?php","",$file);
    $file = str_replace("?>","",$file);
    $file = str_replace("\n","",$file);
    $file = str_replace("\r","",$file);

    $db = array ('wgDBname','wgDBuser','wgDBpassword');
    preg_match('/wgDBname(.*?);/i', $file, $db ['wgDBname']);
    preg_match('/wgDBuser(.*?);/i', $file, $db ['wgDBuser']);
    preg_match('/wgDBpassword(.*?);/i', $file, $db ['wgDBpassword']);

    foreach ($db as $i=>$v){
        if(!is_array($v)){
            continue;
        }
        foreach ($v as $ii=>$vv){
           $db[$i][$ii] = str_replace('"',"",$db[$i][$ii]); 
           $db[$i][$ii] = str_replace('=',"",$db[$i][$ii]);
           $db[$i][$ii] = trim(str_replace('=',"",$db[$i][$ii]));
        }
    }//end foreach
    
    return array (
        'db'=> $db ['wgDBname'][1],
        'user'=> $db ['wgDBuser'][1],
        'pass'=> $db ['wgDBpassword'][1],
    );
    
}//end function
?>