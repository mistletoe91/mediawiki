function showonlyone(thechosenone) {
     $('.jtablcls').each(function(index) {
          if ($(this).attr("id") == thechosenone) {
               $(this).show(200);
          }
          else {
               $(this).hide(600);
          }
     });
}

    $(document).ready(function () {

        $('#firstHeading').html ('Access Control List');
        
        var disclaimer = '<a href="https://www.mediawiki.org/wiki/Security_issues_with_authorization_extensions">Please read the disclaimer regarding implementing Access Level Controls in MediaWiki</a></div>';
        var heading_ = '<div>Use following tabs to add/change/remove restrictions on pages, categories by users and user groups. By default all permissions are open but can be restricted as required. Please note that due to large number of data <strong>the operation might be slow.</strong> Please proceed with patience.'+disclaimer;
        var userpagediv =   '<div id="divuserpage"><a  href="javascript:showonlyone(\'ctldivuserpage\');" ><h3>Add Restrictions To Page By User</h3></a><div id="ctldivuserpage" class="hideme jtablcls"></div></div>';
        var grouppagediv = '<div id="divgrouppage"><a  href="javascript:showonlyone(\'ctldivgrouppage\');" ><h3>Add Restrictions To Page By Group</h3></a><div id="ctldivgrouppage" class="hideme jtablcls"></div></div>';
        var usercatdiv =   '<div id="divusercat">  <a  href="javascript:showonlyone(\'ctldivusercat\');" ><h3>Add Restrictions To Category By User</h3></a><div id="ctldivusercat" class="hideme jtablcls"></div></div>';
        var groupcatdiv =  '<div id="divgroupcat"> <a  href="javascript:showonlyone(\'ctldivgroupcat\');" ><h3>Add Restrictions To Category By Group</h3></a><div id="ctldivgroupcat" class="hideme jtablcls"></div></div>';

        $('#mw-content-text').html ('<div>'+heading_+usercatdiv+groupcatdiv+userpagediv+grouppagediv+'</div>');




        //Load Table
        $('#divuserpage > .jtablcls').jtable({
            title: 'User Page Restrictions',
            actions: {
					listAction: '/custom/api.php?action=restrict_user_page_list',
					createAction: '/custom/api.php?action=restrict_user_page_create',
					updateAction: '/custom/api.php?action=restrict_user_page_update',
					deleteAction: '/custom/api.php?action=restrict_user_page_delete'
            },
            fields: {
                id: {
                    key: true,
                    list: false
                },
                user_id: {
                    title: 'Username',
                    options : "/custom/api.php?action=getallusers",
                    optionsSorting : 'text',
                },
                page_id: {
                    title: 'Page',
                    options : "/custom/api.php?action=getallpages"
                    ,optionsSorting : 'text',
                },
                restrict_edit: {
                    title: 'Edit',
                    options: { '1': 'Restricted', '0': 'Allowed' }
                    ,optionsSorting : 'text',
                },
                restrict_view: {
                    title: 'View',
                    options: { '1': 'Restricted', '0': 'Allowed' }
                    ,optionsSorting : 'text',
                }
            }
        });
        $('#divgrouppage > .jtablcls').jtable({
            title: 'Group Page Restrictions',
            actions: {
					listAction: '/custom/api.php?action=restrict_group_page_list',
					createAction: '/custom/api.php?action=restrict_group_page_create',
					updateAction: '/custom/api.php?action=restrict_group_page_update',
					deleteAction: '/custom/api.php?action=restrict_group_page_delete'
            },
            fields: {
                id: {
                    key: true,
                    list: false
                },
                group_id: {
                    title: 'Group',
                    options : "/custom/api.php?action=getallgroups"
                    ,optionsSorting : 'text',
                },
                page_id: {
                    title: 'Page',
                    options : "/custom/api.php?action=getallpages"
                    ,optionsSorting : 'text',
                },
                restrict_edit: {
                    title: 'Edit',
                    options: { '1': 'Restricted', '0': 'Allowed' }
                },
                restrict_view: {
                    title: 'View',
                    options: { '1': 'Restricted', '0': 'Allowed' }
                }
            }
        });
        $('#divusercat > .jtablcls').jtable({
            title: 'User Category Restrictions',
            actions: {
					listAction: '/custom/api.php?action=restrict_user_cats_list',
					createAction: '/custom/api.php?action=restrict_user_cats_create',
					updateAction: '/custom/api.php?action=restrict_user_cats_update',
					deleteAction: '/custom/api.php?action=restrict_user_cats_delete'
            },
            fields: {
                id: {
                    key: true,
                    list: false
                },
                user_id: {
                    title: 'User',
                    options : "/custom/api.php?action=getallusers"
                    ,optionsSorting : 'text',
                },
                cat_id: {
                    title: 'Category',
                    options : "/custom/api.php?action=getallcats"
                    ,optionsSorting : 'text',
                },
                restrict_edit: {
                    title: 'Edit',
                    options: { '1': 'Restricted', '0': 'Allowed' }
                },
                restrict_view: {
                    title: 'View',
                    options: { '1': 'Restricted', '0': 'Allowed' }
                }
            }
        });
        $('#divgroupcat > .jtablcls').jtable({
            title: 'Group Category Restrictions',
            actions: {
					listAction: '/custom/api.php?action=restrict_group_cats_list',
					createAction: '/custom/api.php?action=restrict_group_cats_create',
					updateAction: '/custom/api.php?action=restrict_group_cats_update',
					deleteAction: '/custom/api.php?action=restrict_group_cats_delete'
            },
            fields: {
                id: {
                    key: true,
                    list: false
                },
                group_id: {
                    title: 'Group',
                    options : "/custom/api.php?action=getallgroups"
                    ,optionsSorting : 'text',
                },
                cat_id: {
                    title: 'Category',
                    options : "/custom/api.php?action=getallcats"
                    ,optionsSorting : 'text',
                },
                restrict_edit: {
                    title: 'Edit',
                    options: { '1': 'Restricted', '0': 'Allowed' }
                },
                restrict_view: {
                    title: 'View',
                    options: { '1': 'Restricted', '0': 'Allowed' }
                }
            }
        });


        $('#divuserpage > .jtablcls').jtable('load');
        $('#divgrouppage > .jtablcls').jtable('load');
        $('#divusercat > .jtablcls').jtable('load');
        $('#divgroupcat > .jtablcls').jtable('load');
        
        //
        //$('#jtable-dropdown-input #Edit-group_id').select2();

    });